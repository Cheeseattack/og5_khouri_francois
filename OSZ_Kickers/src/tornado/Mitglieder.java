package tornado;

public abstract class Mitglieder {
	private String name;
	private String telefonnr;
	private boolean jahresbeitrag;

	public Mitglieder() {}
	
	
	public Mitglieder(String name, String telefonnr, boolean jahresbeitrag) {
		super();
		this.name = name;
		this.telefonnr = telefonnr;
		jahresbeitrag = jahresbeitrag;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTelefonnr() {
		return telefonnr;
	}

	public void setTelefonnr(String telefonnr) {
		this.telefonnr = telefonnr;
	}

	public boolean isJahresbeitrag() {
		return jahresbeitrag;
	}

	public void setJahresbeitrag(boolean jahresbeitrag) {
		this.jahresbeitrag = jahresbeitrag;
	}

}
