package tornado;

public class Spieler extends Mitglieder{

	private short trikotnr;
	private String spielerposition;
	
	
	public Spieler() {} 
	public Spieler(short trikotnr, String spielerposition, String name, String telefonnr, boolean jahresbeitrag){
		super(name, telefonnr, jahresbeitrag);
		this.spielerposition  = spielerposition;
		this.trikotnr = trikotnr; 
		
	}
	public short getTrikotnr() {
		return trikotnr;
	}
	public void setTrikotnr(short trikotnr) {
		this.trikotnr = trikotnr;
	}
	public String getSpielerposition() {
		return spielerposition;
	}
	public void setSpielerposition(String spielerposition) {
		this.spielerposition = spielerposition;
	}
}
