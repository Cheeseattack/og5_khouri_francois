package tornado;

public class Schiedsrichter extends Mitglieder{

	private int spiele;

	public Schiedsrichter() {} 
	
	public Schiedsrichter(int spiele, String name, String telefonnr, boolean jahresbeitrag) {
		super(name, telefonnr, jahresbeitrag);
		this.spiele = spiele; 
		
	}
	
	public int getSpiele() {
		return spiele;
	}

	public void setSpiele(int spiele) {
		this.spiele = spiele;
	} 
}
