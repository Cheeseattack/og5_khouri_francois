package Addon;

public class Addon {

	private String bezeichnung;
	private double verkaufspreis;
	private int aktuellBestand;
	private int maximalBestand;
	private String id_nummer;
	
	public Addon(){
		this.bezeichnung = " " ; 
		this.verkaufspreis = 0.0 ; 
		this.aktuellBestand = 0; 
		this.maximalBestand = 0; 
		this.id_nummer= " ";
	}
	public Addon(String bezeichnung,double verkaufspreis, int aktuellBestand, int maximalBestand, String id_nummer){
		this.bezeichnung = bezeichnung ;
		this.verkaufspreis = verkaufspreis; 
		this.aktuellBestand = aktuellBestand; 
		this.maximalBestand = maximalBestand; 
		this.id_nummer = id_nummer; 
		
	}
	

	public String getId_nummer() {
		return id_nummer;
	}

	public void setId_nummer(String id_nummer) {
		this.id_nummer = id_nummer;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public double getVerkaufspreis() {
		return verkaufspreis;
	}

	public void setVerkaufspreis(double verkaufspreis) {
		this.verkaufspreis = verkaufspreis;
	}

	public int getAktuellBestand() {
		return aktuellBestand;
	}

	public void setAktuellBestand(int aktuellBestand) {
		this.aktuellBestand = aktuellBestand;
	}

	public int getMaximalBestand() {
		return maximalBestand;
	}

	public void setMaximalBestand(int maximalBestand) {
		this.maximalBestand = maximalBestand;
	}
	
	public void bestandveraendern(int wert){ 
		this.aktuellBestand = aktuellBestand - wert; 
}
	public int gebeGesamtwert(int anzahl){
		this.verkaufspreis = verkaufspreis * anzahl;
		return (int) verkaufspreis; 
	}
	}
	

