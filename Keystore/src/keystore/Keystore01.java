package keystore;

public class Keystore01 {
	private String[] keylist;
	private int currentPos; 
	private int counter;;

	public Keystore01() {
		keylist = new String[100];
	}

	public Keystore01(int length) {

	}

	public String get(int index) {
		if (index <= keylist.length - 1)
			return keylist[index];
		else
			return null;

	}

	public int size(){
		
		for (int i = 0; i < keylist.length; i ++){
		    if (keylist[i] != null)
		        counter ++;
		}
		if(counter > Integer.MAX_VALUE)
			return Integer.MAX_VALUE;
		else 
			return counter; 
	}
	
	public void clear(){
		for(int i = 0; i<= keylist.length; i++){
			keylist[i] = null; 
		}
	}
	
	public boolean add (String e){
		if(currentPos>= keylist.length)
			return false; 
		
		else{
			keylist[currentPos]= e;
			currentPos++;
			return true;
		}		
	}
	
	public boolean remove(int index){
		for(int i = 0 ; i < keylist.length; i++){
			if(keylist[i]== keylist[index])
				return true;
			
		}
		return false; 
	}
	
	public boolean remove(String str){
		for (int i=0;i<keylist.length;i++) {
	        final String s = keylist[i];
	        if (str == s || s.contentEquals(str)) {
	            String[] newList = new String[keylist.length];
	            System.arraycopy(keylist, 0, newList ,0, i);
	            if (keylist.length > i+1) {
	                System.arraycopy(keylist, i+1, newList, i, keylist.length - 1 - i);
	            }
	            keylist = newList;
	            counter--;
	            return true;
	         }
	     }
	     return false;
	}
	}

