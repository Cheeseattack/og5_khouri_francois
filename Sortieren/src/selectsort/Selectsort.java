package selectsort;

import java.util.Arrays;

public class Selectsort {
	 
    public static int[] doSelectsort(int[] ray){
         
        for (int i = 0; i < ray.length - 1; i++)
        {
            int index = i;
            for (int j = i + 1; j < ray.length; j++)
                if (ray[j] < ray[index])
                    index = j;
            System.out.print( ray[i]+ " ");
            int kleineNummer = ray[index]; 
            ray[index] = ray[i];
            ray[i] = kleineNummer;
            System.out.println( ray[i]+ " ") ;
        }
        

        return ray;
		
    }
    public static void main(String a[]){
    	
        int[] ray1 = {54,76,8,53,5,74,85,35};
        System.out.println(Arrays.toString(ray1)); 
        int[] ray2 = doSelectsort(ray1);
        System.out.println(Arrays.toString(ray2)); 
       
    }
}