package quicksort;

import java.util.Arrays;

public class Quicksort {
	public static void main(String[] args) {
		int[] a = { 5, 8, 3, 7, 2, 9, 4 };
		System.out.println(Arrays.toString(a));

		int tief = 0;
		int hoch = a.length - 1;
		


		quicksort(a, tief, hoch, a);
		System.out.println(Arrays.toString(a));
	}

	public static void quicksort(int[] ray, int tief, int hoch, int[] a ) {
		if (ray == null || ray.length == 0)
			return;

		if (tief >= hoch)
			return;

		
		int mid = tief + (hoch - tief) / 2;
		int pivot = ray[mid];

		
		int i = tief, j = hoch;
		while (i <= j) {
			while (ray[i] < pivot) {
				i++;
				

			}

			while (ray[j] > pivot) {
				j--;
			}

			if (i <= j) {
				int temp = ray[i];
				ray[i] = ray[j];
				ray[j] = temp;
				i++;
				j--;
				System.out.println(" " + Arrays.toString(a));
				

			}
		}

		
		if (tief < j)
			quicksort(ray, tief, j, a);
	

		if (hoch > i)
			quicksort(ray, i, hoch, a);
	}
}
